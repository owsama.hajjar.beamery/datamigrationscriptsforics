Function ExportWSToCSV ($folderName, $excelFileName) {
    $rootFolder = "C:\localIntegrationApps\dataMigration\convertFiles\"
    $excelFile = $rootFolder + $folderName + "\" + $excelFileName + ".xlsx"
    $E = New-Object -ComObject Excel.Application
    $E.Visible = $false
    $E.DisplayAlerts = $false
    $wb = $E.Workbooks.Open($excelFile)
    foreach ($ws in $wb.Worksheets) {
        # $n = $excelFileName + "_" + $ws.Name
        $n = $ws.Name
        $ws.SaveAs($rootFolder + $folderName + "\" + $n + ".csv", 6)
    }
    $E.Quit()
    # line of code not needed 'stop-process -processname EXCEL'
}