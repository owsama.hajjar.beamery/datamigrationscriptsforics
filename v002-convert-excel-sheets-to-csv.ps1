Function ExportWSToCSV () {
    param(
        [Parameter(Mandatory = $false)][string]$rootFolderPath,
        [Parameter(Mandatory = $false)][string]$excelFileName,
        [Parameter(Mandatory = $false)][string]$excelFileExtension
    )
    $version = "002"
    Clear-Host
    #checking root folder
    if (!($rootFolderPath)) {
        $curFolder = Get-Location
        Write-Host "Assuming current directory as root folder path"
        $rootFolderPath = $curFolder
    }
    #checking excel file name variable
    if (!($excelFileName) -And ([string]::IsNullOrEmpty($excelFileName))) {
        Write-Warning "excelFileName variable is mandetory, please input 'ExportWSToCSV -excelFileName ' followed by the excel file name without extension."
        Write-Host "Aborting..."
        Break
    }
    #checking excel file extension
    if (!($excelFileExtension)) {
        $excelFileExtension = "xlsx"
        $excelFileExtensionDot = "." + $excelFileExtension
    }
    #Creating task report txt file
    $dateTime = Get-Date -UFormat "%Y-%m-%d-T%H-%M-%S"
    $filePath = "$rootFolderPath\ExportWSToCSV_Report_$dateTime.txt"
    "ExportWSToCSV_Report_Version$version" | Out-File -FilePath $filePath
    "DateTime $dateTime" | Out-File -FilePath $filePath -Append
    "" | Out-File -FilePath $filePath -Append
    "Provided Variables:" | Out-File -FilePath $filePath -Append
    "- rootFolderPath: $rootFolderPath" | Out-File -FilePath $filePath -Append
    "- excelFileName: $excelFileName" | Out-File -FilePath $filePath -Append
    "" | Out-File -FilePath $filePath -Append
    # Writing to terminal
    Write-Host "ExportWSToCSV_Report_Version$version"
    Write-Host "Provided Variables:"
    Write-Host "- rootFolderPath: $rootFolderPath"
    Write-Host "- excelFileName: $excelFileName"
    Write-Warning "If variables are incorrect, use CTRL+C to stop the process"
    Write-Host "..."
    #checking excel file exists 
    $excelFile = $rootFolderPath + "\" + $excelFileName + $excelFileExtensionDot
    if (!(Get-Item -Path $excelFile -ErrorAction Ignore)) {
        Write-Warning "file does not exist in directory $rootFolderPath, please check the directory and filename are correct."
        Write-Host "Aborting..."
        "file does not exist in directory $rootFolderPath, please check the directory and filename are correct." | Out-File -FilePath $filePath -Append
        "Aborting..." | Out-File -FilePath $filePath -Append
        break
    }
    # this delay is put in on purpose so ICs have a chance to look at the variables and use CTRL + C to stop the process
    Start-Sleep -Seconds 2
    #opennig excel
    $E = New-Object -ComObject Excel.Application
    $E.Visible = $false
    $E.DisplayAlerts = $false
    $wb = $E.Workbooks.Open($excelFile)
    #converting sheets to csv
    Write-Host "Converting worksheets now..."
    "Converting worksheets now..." | Out-File -FilePath $filePath -Append
    foreach ($ws in $wb.Worksheets) {
        # $n = $excelFileName + "_" + $ws.Name
        $n = $ws.Name
        $ws.SaveAs($rootFolderPath + "\" + $n + ".csv", 6)
        Write-Host "   converted worksheet $n to $n.csv"
        "   converted worksheet $n to $n.csv" | Out-File -FilePath $filePath -Append
    }
    Write-Host ""
    Write-Host "Converting worksheets completed. Bye, remember to thank Owsama, he'd really appreciate it :)"
    "" | Out-File -FilePath $filePath -Append
    "Converting worksheets completed. Bye, remember to thank Owsama, he'd really appreciate it :)" | Out-File -FilePath $filePath -Append
    #Quiting Excel Safely
    $E.Quit()
    $null = [System.Runtime.Interopservices.Marshal]::ReleaseComObject($E) #if I don't put this inside a variable then it outputs a 0
    Remove-Variable -Name E 
    [GC]::Collect()
    [GC]::WaitForPendingFinalizers()
    if ($excel.Workbooks.Count -eq 0) {
        Stop-Process -Name EXCEL -ErrorAction SilentlyContinue
    }
}
